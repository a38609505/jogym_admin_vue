import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import member from './modules/member'
import menu from './modules/menu'
import testData from './modules/test-data'
import feesRate from './modules/fees-rate'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    member,
    menu,
    testData,
    feesRate
}
