const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },

                { id: 'CUSTOMER_ADD', icon: 'el-icon-s-custom', currentName: '가맹점등록', link: '/home-menu/customer/create', isShow: true },
                { id: 'CUSTOMER_LIST', icon: 'el-icon-s-custom', currentName: '가맹점리트스', link: '/home-menu/customer/list', isShow: true },
                { id: 'CUSTOMER_DETAIL', icon: 'el-icon-s-custom', currentName: '가맹점상세', link: '/', isShow: false },
                { id: 'CUSTOMER_EDIT', icon: 'el-icon-s-custom', currentName: '가맹점수정', link: '/', isShow: false },

                { id: 'CALCULATE_ADD', icon: 'el-icon-circle-plus-outline', currentName: '정산등록', link: '/home-menu/calculate/create', isShow: true },
                { id: 'CALCULATE_LIST', icon: 'el-icon-circle-plus-outline', currentName: '정산리스트', link: '/home-menu/calculate/list', isShow: true },
                { id: 'CALCULATE_DETAIL', icon: 'el-icon-circle-plus-outline', currentName: '정산상세', link: '/', isShow: false },
                { id: 'CALCULATE_EDIT', icon: 'el-icon-circle-plus-outline', currentName: '정산수정', link: '/', isShow: false },

                { id: 'FEE_RATE_ADD', icon: 'el-icon-money', currentName: '수수료율등록', link: '/home-menu/fee-rate/create', isShow: true },
                { id: 'FEE_RATE_LIST', icon: 'el-icon-money', currentName: '수수료율리스트', link: '/home-menu/fee-rate/list', isShow: true },
                { id: 'FEE_RATE_DETAIL', icon: 'el-icon-money', currentName: '수수료율상세', link: '/', isShow: false },
                { id: 'FEE_RATE_EDIT', icon: 'el-icon-money', currentName: '수수료율수정', link: '/', isShow: false },
            ]
        },
        {
            parentName: '마이 메뉴',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
