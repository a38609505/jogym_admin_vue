import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FEES_LIST]: (store) => {
        return axios.get(apiUrls.DO_FEES_LIST)
    },
}
