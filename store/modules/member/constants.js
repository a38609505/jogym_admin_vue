export default {
    DO_LIST: 'member/doList',
    DO_LIST_PAGING: 'member/doListPaging',
    DO_DETAIL: 'member/doDetail',
    DO_CREATE_COMMENT: 'member/doCreateComment',
    DO_UPDATE: 'member/doUpdate',
    DO_DELETE: 'member/doDelete',
    DO_CREATE: 'member/doCreate'
}
